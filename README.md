# ReBlogSum

ReBlogSum is a live blog summarization platform 

## Technology Used

- [Apache Kafka](https://kafka.apache.org/)
- [lxml](https://pypi.org/project/lxml/)
- [kafka-python](https://pypi.org/project/kafka-python/)
- [KafkaJS](https://kafka.js.org/)
- [Google Cloud Platform](https://cloud.google.com/)
- [ExpressJS](http://expressjs.com/)
- [ReactJS](https://reactjs.org/)
- [Axios](https://axios-http.com/)

## Main Framework

[Live Blog Corpus for Summarization by Avinesh et al. (2018))](https://github.com/AIPHES/live-blog-summarization)

## How to setup

### For Local

The system is best run on Linux Debian

1. Have Kafka Server Running on local
2. Create `.env` file with format: `KAFKA_SERVERS=<ip address of kafka server(s)>`
3. Put the `.env` file into `worker/src` and `backend` directory