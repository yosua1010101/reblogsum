const {Kafka} = require('kafkajs')
const random = (length = 16) => {
  // Declare all characters
  let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  // Pick characers randomly
  let str = '';
  for (let i = 0; i < length; i++) {
      str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;

};
const delay = (t, val)=>{
  return new Promise((resolve)=>{
    setTimeout(()=>{
      resolve(val)
    },t)
  })
}

testBrokers = ["104.154.206.114:9092", "34.69.5.73:9092", "34.133.123.183:9092"]
const kafka = new Kafka({
    clientId: 'my-app',
    brokers: testBrokers
  })
  
const producer = kafka.producer()

const steps = {
  start: async () => {
    await producer.connect()
    return steps.doSomething()
  },
  doSomething: async()=>{
    var count = 0
    while(true){
      var message = `${count} - ${random()}`
      console.log(message);
      await producer.send({
        topic: 'kafka-test',
        messages: [{
          value: JSON.stringify({message: message}),
          partition: count%3
        }]
      })
      await delay(100);
      count++;
    }
  },
  exit: async () => {
    await producer.send({
      topic: 'test-topic',
      messages: [{
        value: JSON.stringify({message: 'exit'}),
      }]
    })
    await producer.disconnect()
  }
}
steps.start()

// const run = async () => {
// // Producing
//   await producer.connect()
//   console.log('connected')
//   try {
//     const answer = await promise('message: ')
//     console.log(answer)
//   } catch (error) {
//     console.error('something wrong', error)
//   }
//   var number = 0
//   while(number < 9){
//     await producer.send({
//       topic: 'test-topic',
//       messages: [{
//         value: answer,
//       }]
//     })
//     console.log(`reached ${number}`);
//     number++
//   }
//   console.log('reached 10')
//   await producer.send({
//     topic: 'test-topic',
//     messages: [{
//       value: 'exit',
//     }]
//   })
//   await producer.disconnect()
//   console.log('disconnected')
  
//   return new Promise((resolve, reject)=>{
//     resolve(answer), reject(console.error())
//   })
//   // var send_message = ()=> {
//   //     readline.question('message: ', async(message) => {
//   //       await producer.send({
//   //         topic: 'test-topic',
//   //         messages: [
//   //           { value: message },
//   //         ],
//   //       })
//   //       if(message !== 'exit'){
//   //         send_message()
//   //       } else {
//   //         producer.disconnect().then(()=>{
//   //           console.log('exited 1')
//   //           return new Promise((resolve, reject)=>{
//   //             resolve('exited 2')
//   //           })
//   //         })
//   //       }
//   //     })
//   //   }
//   // send_message()
// }

// run().then((answer)=>{console.log(answer);}).catch(console.error())
