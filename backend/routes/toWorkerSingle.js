var express = require('express');
var router = express.Router();
const {Kafka} = require('kafkajs')
brokers = process.env.KAFKA_SERVERS.split(',')
fetchTopic = process.env.FETCHER_TOPIC
console.log(fetchTopic)
const random = (length = 16) => {
    // Declare all characters
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    // Pick characers randomly
    let str = '';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;

};

const kafka = new Kafka({
    clientId: 'reblogsum',
    brokers: brokers
  })

const producer = kafka.producer()


router.post('/upload', async function(req, res, next){
    if(req.method == 'POST'){
        var sessionID = random()
        var data = req.body
        data.sessionID = sessionID
        console.log(`${data.sessionID} - ${data.lowerRange} - ${data.upperRange}`)
        await producer.connect()
        await producer.send({
            topic: fetchTopic,
            messages:[{
                value: JSON.stringify(data),
                key: sessionID
                }]
        })
        await producer.disconnect()
        res.locals.sessionID = sessionID
        next()
        }
    }, async function(req, res, next){
        var consumer = kafka.consumer({ groupId: `reblogsum-urlcount-${res.locals.sessionID}` })
        await consumer.connect()
        try { 
            await consumer.subscribe({ topic: `urlcount-${res.locals.sessionID}`, fromBeginning: true })
            await consumer.run({
                eachMessage: async ({ topic, partition, message })=>{
                    res.locals.urlCount = JSON.parse(message.value.toString()).urlCount
                    consumer.disconnect().then(()=>{
                        next()
                    })
                }
            })
        } catch{
            consumer.disconnect().then(()=>{
                res.status(500).send("Something error with KafkaJS")
            })
        }
    }, async function(req, res, next){
        var consumer2 = kafka.consumer({ groupId: `reblogsum-summary-${res.locals.sessionID}` })
        await consumer2.connect()
        var summaryList = []
        try {
            await consumer2.subscribe({ topic: `summary-${res.locals.sessionID}`, fromBeginning: true })
            await consumer2.run({
                eachMessage: async({ topic, partition, message })=>{
                    summaryList.push(JSON.parse(message.value.toString()))
                    if(summaryList.length >= res.locals.urlCount){ 
                        res.locals.summaryList = summaryList
                        consumer2.disconnect().then(()=>{
                            next()
                        })
                    }
                }
            })
        } catch {
            consumer2.disconnect().then(()=>{
                res.status(500).send("Something error with KafkaJS")
            })
        }
    }, function(req,res,next){
        res.status(200).send({data: res.locals.summaryList})
    })

// router.post('/upload', function(req, res, next){
//     if(req.method == 'POST'){
//         uploadSteps.start(req.body, res)
//     }
// })

module.exports = router;