const {Kafka} = require('kafkajs')
testBrokers = ["104.154.206.114:9092", "34.69.5.73:9092", "34.133.123.183:9092"]
const kafka = new Kafka({
    clientId: 'reblogsum-test-js',
    brokers: testBrokers
  })
  
const consumer = kafka.consumer({ groupId: 'test-group' })

const consumeSteps = {
    start: async ()=>{
        await consumer.connect()
        await consumer.subscribe({ topic: 'kafka-test' })
        return consumeSteps.consume()
    },
    consume: async()=>{
        await consumer.run({
            eachMessage: async ({topic, partition, message})=>{
                const obj = JSON.parse(message.value.toString())
                console.log(`${topic} - ${partition}`)
                console.log(obj)
            }
        })
    },
    exit: async()=>{
        consumer.disconnect()
    }
}
consumeSteps.start()

// const run = async () => {
// // Consuming
//     await consumer.connect()
//     await consumer.subscribe({ topic: 'test-topic'})
//     await consumer.run({
//         eachMessage: async ({ topic, partition, message }) => {
//             a = message.value.toString()
//             console.log(a)
//             if (a === "exit"){
//                 consumer.disconnect()
//             }
//         },
//     })
// }

// run().catch(console.error)