from kafka import KafkaAdminClient, admin
import re
import dotenv
import os

dotenv.load_dotenv()

bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value
fetch_topic = os.getenv('FETCHER_TOPIC')
summarize_topic = os.getenv('SUMMARIZER_TOPIC')
topics = [fetch_topic, summarize_topic]
admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)

if (fetch_topic in admin_client.list_topics()):
    admin_client.delete_topics([fetch_topic])
if (summarize_topic in admin_client.list_topics()):
    admin_client.delete_topics([summarize_topic])
admin_client.delete_topics([x for x in admin_client.list_topics() if (re.match('summary-.*', x))])
admin_client.delete_topics([x for x in admin_client.list_topics() if (re.match('urlcount-.*', x))])