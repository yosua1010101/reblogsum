from kafka import KafkaAdminClient
from kafka.admin import  NewTopic
from kafka.errors import  TopicAlreadyExistsError
import re
import dotenv
import os

dotenv.load_dotenv()

bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value
admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)

topic_partitions = {}
fetch_topic = os.getenv('FETCHER_TOPIC')
summarize_topic = os.getenv('SUMMARIZER_TOPIC')
topics = [fetch_topic, summarize_topic]

try:
    admin_client.create_topics(new_topics=[NewTopic(topic,3,1) for topic in topics])
except TopicAlreadyExistsError as e:
    print(e)
    print()
finally:
    kafka_topics = admin_client.describe_topics(topics)
    for topic in kafka_topics:
        print(topic['topic'])
        for partition in topic['partitions']:
            print(partition)