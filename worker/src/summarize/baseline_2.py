import sys
import os
import pathlib
import argparse
import logging
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils.misc import set_logger
from utils.data_helpers import load_data

from summarize.upper_bound import ExtractiveUpperbound
from summarize.sume_wrap import SumeWrap
from summarize.sumy.nlp.tokenizers import Tokenizer
from summarize.sumy.parsers.plaintext import PlaintextParser
from summarize.sumy.summarizers.lsa import LsaSummarizer
from summarize.sumy.summarizers.kl import KLSummarizer
from summarize.sumy.summarizers.luhn import LuhnSummarizer
from summarize.sumy.summarizers.lex_rank import LexRankSummarizer
from summarize.sumy.summarizers.text_rank import TextRankSummarizer
from summarize.sumy.nlp.stemmers import Stemmer
from nltk.corpus import stopwords
from rouge.rouge import Rouge

logger = logging.getLogger(__name__)
SUMY_CONSTANT = 20

def get_args():
    ''' This function parses and return arguments passed in'''

    parser = argparse.ArgumentParser(description='Upper Bound for Summarization')
    # -- summary_len: 100, 200, 400
    parser.add_argument('-s', '--summary_size', type=str, help='Summary Length ex:100', required=False)

    # --data_set: DUC2001, DUC2002, DUC2004
    parser.add_argument('-d', '--data_set', type=str, help='Data set ex: bbc, guardian', required=True)

    # --language: english, german
    parser.add_argument('-l', '--language', type=str, help='Language: english, german', required=False,
                        default='english')

    parser.add_argument('-io', '--iobasedir', type=str, help='IO base directory', required=False,
                        default=pathlib.Path.cwd() / "data")
    args = parser.parse_args()

    return args

def get_summary(algo:str, docs, refs, summary_size, language):
    summaries = []
    docs.reverse()
    for doc in docs:
        if algo == 'UB1':
            summarizer = ExtractiveUpperbound(language)
            summary = summarizer(doc, refs, summary_size, ngram_type=1)
        elif algo == 'UB2':
            summarizer = ExtractiveUpperbound(language)
            summary = summarizer(doc, refs, summary_size, ngram_type=2)
        elif algo == 'ICSI':
            summarizer = SumeWrap(language)
            summary = summarizer(doc, summary_size)
        else:
            doc_string = u". ".join([sentence for sentence in doc])
            parser = PlaintextParser.from_string(doc_string, Tokenizer(language))
            stemmer = Stemmer(language)
            if algo == 'LSA':
                summarizer = LsaSummarizer(stemmer)
            if algo == 'KL':
                summarizer = KLSummarizer(stemmer)
            if algo == 'Luhn':
                summarizer = LuhnSummarizer(stemmer)
            if algo == 'LexRank':
                summarizer = LexRankSummarizer(stemmer)
            if algo == 'TextRank':
                summarizer = TextRankSummarizer(stemmer)

            summarizer.stop_words = frozenset(stopwords.words(language))
            summary_size = max(summary_size // SUMY_CONSTANT, 1)
            summary = summarizer(parser.document, summary_size)
        summaries.append(" ".join(summary))
    return summaries
    # best_summary = {"sentence":None, "f1-score":0}
    # for sum_sentence in summary:
    #     sum_score = 0
    #     for ref_sentence in refs[0]:
    #         rouge_score = rouge.get_scores(sum_sentence, ref_sentence)[0]['rouge-l']['f']
    #         if rouge_score > sum_score:
    #             sum_score = rouge_score
    #     if sum_score > best_summary["f1-score"]:
    #         best_summary = {"sentence":sum_sentence, "f1-score":sum_score}
    # return best_summary


def main():
    args = get_args()
    data_dir = pathlib.Path.cwd() / "data"
    data_path = data_dir / f"raw/downloads/{args.data_set}/"
    log_path = data_dir / "logs/"
    log_file = log_path / f'baselines_{args.data_set}.log'

    log_path.mkdir(parents=True, exist_ok=True)
    set_logger(str(log_file))

    for filename in data_path.iterdir():
        topic_id = filename.stem
        docs, refs = load_data(str(filename))
        if not refs:
            continue

        if not args.summary_size:
            summary_size = len(" ".join(refs[0]).split(' '))
        else:
            summary_size = int(args.summary_size)
        
        logger.info(f'Topic ID: {topic_id}')
        logger.info('###')
        logger.info(f'Summary Length: {summary_size}')
        rouge = Rouge(metrics=list(Rouge.AVAILABLE_METRICS.keys()), stats=["f"])
        algos = ['UB1', 'UB2', 'ICSI', 'Luhn', 'LexRank', 'TextRank', 'LSA', 'KL']
        for algo in algos:
            summaries = get_summary(algo, docs, refs, summary_size, args.language)
            paragraph = ' '.join(summaries)
            logger.info(f"Summary according to {algo}: {paragraph}")
        logger.info('###')

if __name__ == '__main__':
    main()