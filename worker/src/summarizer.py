import dotenv
import os
import json
from summarize.upper_bound import ExtractiveUpperbound
from kafka import KafkaConsumer, KafkaProducer, TopicPartition
from utils.data_helpers import load_data
import nltk
import pathlib

nltk.download('stopwords')
nltk.download('punkt')
dotenv.load_dotenv()
bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value
LANGUAGE = 'english'
SUMMARY_SIZE = 200

consumer = KafkaConsumer(bootstrap_servers=bootstrap_servers, group_id='reblogsum-summarizer',
                         value_deserializer=lambda json_str: json.loads(json_str.decode('utf-8')))

producer = KafkaProducer(bootstrap_servers=bootstrap_servers, 
                         value_serializer=lambda dct: json.dumps(dct).encode('utf-8'))



def upper_bound(docs, refs):
    summarizer = ExtractiveUpperbound(LANGUAGE)
    return summarizer(docs, refs, SUMMARY_SIZE)

def summary_exists(sum_file):
    with open(sum_file, 'r' ) as json_file:
        return json.load(json_file)

def summary_does_not_exist(blog_id, docs, refs, timestamp, blog_title):
    docs.reverse()
    summary_paragraph = '. '.join(upper_bound(docs, refs))
    summary_metadata = {'blog_id': blog_id, 'title': blog_title, 'summary': summary_paragraph, 'timestamp': timestamp}
    with open(f'sum_cache/{blog_id}.json', 'w') as file_write:
        json.dump(summary_metadata, file_write)
    return summary_metadata

def check_and_process(blog_id, docs, refs, timestamp, blog_title):
    sum_file = f'sum_cache/{blog_id}.json'
    if pathlib.Path(sum_file).exists():
        summary_metadata = summary_exists(sum_file)
    else:
        summary_metadata = summary_does_not_exist(blog_id, docs, refs, timestamp, blog_title)
    summary_metadata['timestamp'][1] -= 1
    return summary_metadata

def main():
    consumer.subscribe(os.getenv('SUMMARIZER_TOPIC').split(','))
    print('Summarizer ready')
    try:
        for msg in consumer:
            blog_id, docs, refs, timestamp, blog_title, session_id = load_data(msg.value)
            print(f'{msg.partition} - {session_id} - {blog_id}')
            summary_metadata = check_and_process(blog_id, docs, refs, timestamp, blog_title)

            producer.send(topic=f'summary-{session_id}', value=summary_metadata)
            producer.flush()

    except:
        consumer.unsubscribe()
    
if __name__ == '__main__':
    main()


