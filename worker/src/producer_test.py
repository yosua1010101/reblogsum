from kafka import KafkaProducer
import dotenv
import os
import time

dotenv.load_dotenv()
print(os.getenv('KAFKA_SERVERS').split(','))
bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value list separated with comma

producer = KafkaProducer(bootstrap_servers=bootstrap_servers,
                         value_serializer=lambda x: x.encode('utf-8'))

def main():
    try:
        count = 0
        while True:
            print(f'{count} - test')
            producer.send('test-topic', value=f'{count} - test', partition=count%3)
            producer.flush()
            time.sleep(1)
            count += 1
    except KeyboardInterrupt:
        print("Keyboard Interrupted")

if __name__ == '__main__':
    main()