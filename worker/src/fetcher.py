import dotenv
import os
from crawler._corpus import AbstractCorpus
from crawler.guardian import Guardian
from kafka import KafkaProducer, KafkaConsumer, TopicPartition
from datetime import date
import hashlib
import json
import nltk
import pathlib

nltk.download('stopwords')
nltk.download('punkt')
dotenv.load_dotenv()
bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value
fetch_topic = os.getenv('FETCHER_TOPIC')
summarize_topic = os.getenv('SUMMARIZER_TOPIC')

producer = KafkaProducer(bootstrap_servers=bootstrap_servers, 
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))

consumer = KafkaConsumer(bootstrap_servers=bootstrap_servers, group_id='reblogsum-fetcher',
                         value_deserializer=lambda json_str: json.loads(json_str.decode('utf-8')))

def hash_hex(s: bytes) -> str:
    h = hashlib.sha1()
    h.update(s)
    return h.hexdigest()

def check_metadata_exists(filepath: str) -> bool:
    with open(filepath, 'r') as json_file:
        return False if json.load(json_file) is None else True

def fetch(corpora_builder: Guardian, url: str) -> dict:
    try:
        json_doc = corpora_builder.download_html(url)
        return json_doc
    except AbstractCorpus.LowQualityError:
        return None
    except AbstractCorpus.LiveOngoingError:
        return None
    except TypeError:
        return None
    except IndexError:
        return None

def extract_message(msg):
    begin_date = date.fromisoformat(msg.value['lowerRange'])
    end_date = date.fromisoformat(msg.value['upperRange'])
    session_id = msg.value['sessionID']
    if begin_date > end_date:
        begin_date, end_date = end_date, begin_date
    return begin_date,end_date,session_id

def check_and_process(session_id, corpora_builder, url):
    fetch_metadata = None
    blog_id = hash_hex(url.encode("utf-8"))
    print(blog_id)
    filepath = f'fetch_cache/{blog_id}.json'
    if pathlib.Path(filepath).exists() and check_metadata_exists(filepath):
        fetch_metadata = corpus_exists(filepath)
    else:
        fetch_metadata = corpus_does_not_exist(corpora_builder, url, filepath)
    if fetch_metadata is not None:
        fetch_metadata['session_id'] = session_id
        producer.send(topic=summarize_topic, value=fetch_metadata, key=session_id.encode('utf-8'))
        producer.flush()
    return fetch_metadata

def corpus_exists(filepath):
    with open(filepath, 'r') as json_file:
        return json.load(json_file)

def corpus_does_not_exist(corpora_builder, url, filepath):
    fetch_metadata = fetch(corpora_builder, url)
    with open(filepath, 'w') as file_write:
        json.dump(fetch_metadata, file_write)
    return fetch_metadata


def main():
    consumer.subscribe(fetch_topic.split(','))
    print("Fetcher Ready")
    try:
        for msg in consumer:
            begin_date, end_date, session_id = extract_message(msg)
            print(f'{msg.partition} - {session_id}')
            corpora_builder = Guardian()
            urls = corpora_builder.fetch_url(begin_date, end_date)
            data = []
            
            for url in urls:
                fetch_metadata = check_and_process(session_id, corpora_builder, url)
                data.append(fetch_metadata)

            cleaned_data = list(filter(None, data))
            sent_url_count = len(cleaned_data)
            producer.send(topic=f'urlcount-{session_id}', value={'urlCount': sent_url_count})
            producer.flush()

    except:
        consumer.unsubscribe()

if __name__ == "__main__":
    main()