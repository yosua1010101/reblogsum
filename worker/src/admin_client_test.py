from kafka import KafkaAdminClient
from kafka.admin import NewTopic
import dotenv
import os
import time

dotenv.load_dotenv()
print(os.getenv('KAFKA_SERVERS').split(','))
bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value list separated with comma

admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)

if 'test-topic' in admin_client.list_topics():
    admin_client.delete_topics(['test-topic'])
    time.sleep(3)
admin_client.create_topics(new_topics=[NewTopic('test-topic',3,1)])
[print(partition) for partition in admin_client.describe_topics(topics=['test-topic'])[0]['partitions']]