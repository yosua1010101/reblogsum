from kafka import KafkaConsumer
import dotenv
import os

dotenv.load_dotenv()
print(os.getenv('KAFKA_SERVERS').split(','))
bootstrap_servers = os.getenv('KAFKA_SERVERS').split(',') # create ".env" file with "KAFKA_SERVERS" as key and <kafka server(s) IP address>:9092 as value

consumer = KafkaConsumer(bootstrap_servers=bootstrap_servers, group_id='test-group',
                         value_deserializer=lambda x: x.decode('utf-8'))

def main():
    consumer.subscribe(['test-topic'])
    print(consumer.assignment())
    try:
        for msg in consumer:
            print(msg.partition)
            print(msg.value)
    except:
        consumer.unsubscribe()
if __name__ == '__main__':
    main()