from ._corpus import AbstractCorpus
from abc import ABC
from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
from lxml import html
from multiprocessing import Pool

from nltk.tokenize import sent_tokenize


import itertools
import re
import requests
import socket


class Guardian(AbstractCorpus, ABC):


    def __init__(self):
        pass
    
    def _get_timelines(self, timeline_links, timeline_block):
        timeline = timeline_block[0]
        for a in timeline.xpath(".//ul/li"):
            b = a.xpath("a")[0]
            timeline_data = {"block": b.get('data-event-id'), 
                             "text": super(Guardian, self).normalize_text(b.text_content()).strip()}
            timeline_links.append(timeline_data)
        return timeline_links

    def _extract_keypoints(self, node):
        sum_now = {"text": [], "link": []}
        sum_now["text"] = super(Guardian, self).normalize_text(super(Guardian, self).normalize_summary(node.text_content()))
        for sub_node in node.xpath(".//a"):
            sum_now["link"].append(sub_node.get("href"))
        return sum_now

    def _get_summary(self, tree):
        summary = {"bulletin": [], "key_events": []}
        content_standfirst = tree.xpath('//div[@class="content__standfirst"]')

        if len(content_standfirst) == 0:
            return summary

        timeline_links = []
        timeline_block = tree.xpath('//div[@data-component="timeline"]')
        if len(timeline_block) == 1:
            timeline_links = self._get_timelines(timeline_links, timeline_block)
        for i in content_standfirst[0].getchildren():
            type_ = i.tag
            if type_ == "p":
                sum_now = self._extract_keypoints(i)
                summary["bulletin"].append(sum_now)
            elif type_ == "ul":
                for j in i.xpath(".//li"):
                    sum_now = self._extract_keypoints(j)
                    summary["bulletin"].append(sum_now)
        summary["key_events"].extend(timeline_links)
        return summary
    
    def _format_date(self, date_hour, datetime_format):
        timestamp = datetime.strptime(date_hour, datetime_format)
        return timestamp.year, timestamp.month, timestamp.day, timestamp.hour, timestamp.minute, timestamp.second

    def _extract_time(self, article):
        block_time = article.xpath('.//p[@class="block-time published-time"]')
        if len(block_time) == 0:
            return None
        cc = block_time[0].xpath(".//time")
        datetime = cc[0].attrib['datetime']
        if datetime is not None:  # "2014-07-23T12:02:45.546Z"
            time_creation = self._format_date(datetime, '%Y-%m-%dT%H:%M:%S.%fZ')
        return time_creation
    
    def _extract_title(self, article):
        part_title = article.xpath('.//h2[@class="block-title"]')
        block_title = ''
        if len(part_title) != 0:
            block_title = part_title[0].text_content()
        return block_title

    def _extract_text(self, article):
        text_lines = []
        block_lines = article.xpath('.//div[@itemprop="articleBody"]')
        for lines in block_lines:
            html_string = html.tostring(lines)
            soup = BeautifulSoup(html_string, "html.parser")
            figure = soup.find_all("figure")
            if figure:
                for match in figure:
                    match.decompose()
            text_line = soup.get_text()
            text_lines = re.split(u'\. ', super(Guardian, self).normalize_text(text_line))
        return text_lines

    def _extract_documents(self, articles):
        body = []
        if len(articles) == 0:
            return body
        
        for article in articles:
            block_id = article.get('id')
            if block_id is None:
                continue
            time_creation = self._extract_time(article)
            block_title = self._extract_title(article)
            text_lines = self._extract_text(article)
            is_key_event = False
            section = article.get("class")
            if re.search('is-key-event|is-summary', section):
                is_key_event = True
            block_text = [sent_tokenize(line.strip()) for line in text_lines if line.strip() != u""]
            block_text_list = list(itertools.chain.from_iterable(block_text))
            d_block = {"time": time_creation, "text": block_text_list, "block_id": block_id,
                        "title": super(Guardian, self).normalize_text(block_title), 'is_key_event': is_key_event}
            body.append(d_block)
        return body
    
    def _get_genre(self, url):
        return url.split('/')[3]

    def _get_documents(self, tree):
        article = tree.xpath('.//div[@itemprop="liveBlogUpdate"]')
        documents = self._extract_documents(article)
        if not documents:
            article = tree.xpath('.//div[@itemprop="articleBody"]')
            documents = self._extract_documents(article)
        return documents

    def _process_html(self, blog_id: str, url: str, html_content: bytes) -> dict:
        tree = html.document_fromstring(html_content.decode("utf-8"))
        if tree.xpath("//header[contains(concat(' ', normalize-space(@class), ' '), ' tonal__head--tone-live ')]"):
            raise super(Guardian, self).LiveOngoingError()
        title = tree.xpath("//title/text()")
        genre = self._get_genre(url)
        time_block = tree.xpath("//time[contains(concat(' ', normalize-space(@class), ' '), ' content__dateline-wpd ')]")
        timestamp = self._format_date(time_block[0].attrib['datetime'], '%Y-%m-%dT%H:%M:%S%z')
        summary = self._get_summary(tree)
        summary_text = [super(Guardian, self).normalize_summary(key_event['text']) for key_event in summary['key_events']]
        if len(summary_text) <= 2:
            raise super(Guardian, self).LowQualityError()
        documents = self._get_documents(tree)
        data = {'blog_id': blog_id, 'url': url, 'genre': genre, 'timestamp': timestamp, 
            'title': title[0], 'summary': summary_text, 'documents': documents}
        return data

    def _next_page(self, content):
        tree = html.fromstring(content)
        older_part = tree.xpath('//div[@class="liveblog-navigation__older"]')
        url = ''
        if len(older_part) >= 1:
            older_part_link = older_part[0].xpath(".//a")[0].get("href")
            if older_part_link:
                url = "http://www.theguardian.com" + older_part_link[:older_part_link.rfind('#')]
        return url

    def _fetch_more(self, url: str, content: bytes, json_docs: list):
        while(True):
            url = self._next_page(content)
            if not url:
                break
            req = requests.get(url, allow_redirects=True, timeout=5)
            if req.status_code != requests.codes.ok:
                break
            content = req.text.encode(req.encoding)
            tree = html.fromstring(content)
            block_data = self._get_documents(tree)
            for item in block_data:
                json_docs.append(item)
        return json_docs

    def download_html(self, url):
        try:
            blog_id = super(Guardian, self).hash_hex(url.encode('utf-8'))
            req = requests.get(url, allow_redirects=True, timeout=5)
            if req.status_code == requests.codes.ok:
                content = req.text.encode(req.encoding)
                json_data = self._process_html(blog_id, url, content)
                json_data['documents'] = self._fetch_more(url, content, json_data['documents'])
            elif(req.status_code in [301, 302, 404, 503]):
                pass
            return json_data
        except requests.exceptions.ConnectionError:
            return None
        except requests.exceptions.ContentDecodingError:
            return None
        except requests.exceptions.ChunkedEncodingError:
            return None
        except requests.exceptions.Timeout:
            return None
        except socket.timeout:
            return None
    

    def _get_url(self, tree):
        urls = []
        pattern = "www.theguardian.com"
        for item in tree.xpath("//div[contains(concat(' ', normalize-space(@data-link-name), ' '), 'dead')]//a[@tabindex]"):
            url = item.get("href")
            genre = self._get_genre(url)
            if re.search(pattern, url) \
                and url not in urls \
                and genre not in super(Guardian, self).LOW_QUALITY_GENRE:
                urls.append(url)
        return urls
    
    def _process_pool(self, url):
        req = requests.get(url, allow_redirects=True, timeout=5)
        if req.status_code != requests.codes.ok:
            return
        content = req.text.encode(req.encoding)
        tree = html.fromstring(content)
        urls = self._get_url(tree)
        return urls

    def fetch_url(self, begin_date: date, end_date: date) -> list:
        url_portals = []
        date_decrement = timedelta(days=1)

        while(end_date >= begin_date):
            guardian_year = end_date.year
            guardian_month = end_date.strftime("%b").lower()
            guardian_day = end_date.strftime("%d")
            url_portals.append(f'https://www.theguardian.com/tone/minutebyminute/{guardian_year}/{guardian_month}/{guardian_day}/all')
            end_date -= date_decrement

        with Pool(6) as pool:
            res = pool.map(self._process_pool, url_portals)
            return [url for urls in res for url in urls]