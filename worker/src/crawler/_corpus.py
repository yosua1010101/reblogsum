from abc import ABC, abstractmethod
import hashlib
import re

class AbstractCorpus(ABC):
    LOW_QUALITY_GENRE = ['sport', 'football', 'cricket', 'tv-and-radio']
    web_driver = None
    max_attempts = 5
    timeout = 5

    def __init__(self):
        pass

    def hash_hex(self, s: bytes):
        h = hashlib.sha1()
        h.update(s)
        return h.hexdigest()

    class LiveOngoingError(Exception):
        def __init__(self, message="The Live Update is Still Ongoing"):
            self.message = message
            super().__init__(self.message)

    class LowQualityError(Exception):
        def __init__(self, message="This Live Article has Low Quality"):
            self.message = message
            super().__init__(self.message)

    def normalize_text(self, text):
        text = re.sub(u"twitter: [^\s]+\s", u'', text)
        text = re.sub(u"twitter: ", u'', text)  
        text = re.sub(u"http[^ \n]+", u'', text)
        text = re.sub(u"@[a-zA-Z\d]+", u'@twitterid', text)
        text = re.sub(u'^[\n_]+',u'', text)
        text = re.sub(u'\u201e|\u201d',u'"', text)
        text = re.sub(u'\u201c',u'"', text)
        text = re.sub(u"\u2022",u'. ', text)  
        text = re.sub(u"([.?!]);",u"\\1", text)
        text = re.sub(u'``', u'``', text)
        text = re.sub(u"\.\.+",u" ", text)
        text = re.sub(u"\s+\.",u".", text)
        text = re.sub(u"\?\.",u"?", text)
        text = re.sub(u"Read more", u"", text)
        #Dash to remove patterns like NAME (Twitter id)
        text = re.sub(u"\u2014[^\n]+", u'', text)
        
        #Line of format Month day, year (ex:March 7, 2017)
        text = re.sub(u"\n[a-zA-Z]+\s+\d+,\s+\d{4}", u'', text)
        
        #Line of format Time GMT (ex:6.20pm GMT)
        text = re.sub(u"\d+\.\d+(am|pm) (GMT|BST)\n", u'', text)
        #Line of format 15:35
        text = re.sub(u"\d+:\d+\n", u'', text)
        
        text = re.sub(u"pic[^ \n]+", u'', text)

        #text = re.sub(u'[\s\t]+',u' ', text)
        text = re.sub(u'[\n_]+',u'\n', text)
        text = re.sub(u"[*]",u"", text)
        text = re.sub(u"\-+",u"-", text)
        text = re.sub(u'^ ',u'', text)
        text = re.sub(u'\[\.*\]',u'', text)

        text = re.sub(u'\u00e2\u0080\u0093',u"", text) 
        text = re.sub(u'\u00c4\u0178',u"\u011f", text) # ğ
        text = re.sub(u'\u0080\u009c',u"", text)
        text = re.sub(u'\u0080\u009d',u"", text)
        text = re.sub(u'\u0080\u0098',u"'", text)
        text = re.sub(u'\u0080\u0099',u"'", text)
        text = re.sub(u'\u0080\u0093',u"", text)
        text = re.sub(u'\u00E2',u'', text)
        text = re.sub(u'\u0080\u0094',u'', text)
        text = re.sub(u'\u00c3\u00b3',u'', text)
        text = re.sub(u'\u20ac\u0153|\u20ac\u009d',u'"',text)
        text = re.sub(u'\u0060\u0060',u'"',text)
        text = re.sub(u'\u00E0',u'a', text)
        text = re.sub(u'\u00E9',u'e', text)
        text = re.sub(u'\u20ac\u2122',"'", text)
        text = re.sub(u'\u2019',u"'", text)
        text = re.sub(u'\u2018',u"'", text)
        text = re.sub(u'\u2013',u'-', text)
        text = re.sub(u'\u20ac',u'-',text)
        text = re.sub(u'\u2026',u'', text)
        text = re.sub(u"\u00A3",u"\u00A3 ", text)
        text = re.sub(u"\nBBC ",u"", text)
        text = re.sub(u"^BBC ",u"", text)
        text = re.sub(u"\. BBC ",u". ", text)
        text = re.sub(u"([.?!]);",u"\\1", text)
        text = re.sub(u'[\n\s\t\r_]+', u' ', text)
        text = re.sub(u"\\u00A0", u" ", text)
        text = re.sub(u"\u00A0", u" ", text) 
        text = re.sub(u' +$', u'', text)  
        text = re.sub(u" {2,10}", u". ", text)
        text = re.sub(u'\xa0', u' ', text)
        text = re.sub(u' +$', u'', text)  
        return text

    def normalize_summary(self, summary):
        summary = self.normalize_text(summary)
        summary = re.sub(u" {2,10}", u". ", summary)
        if summary != u"":
            summary = re.sub(u'[a-zA-Z]$', summary[-1] + ".", summary)       
        return summary

    @abstractmethod
    def _get_summary(self, tree):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _get_timelines(self, timeline_links, timeline_block):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _extract_keypoints(self, node, type_):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _format_date(self, date_hour):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _extract_title(self, article):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _extract_text(self, article):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _extract_time(self, article):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _extract_documents(self, articles):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _get_genre(self, url):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _get_documents(self, tree):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _process_html(self, blog_id, url, html_content):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _next_page(self, content):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _fetch_more(self, url, content, json_docs):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def download_html(self, url):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def _get_url(self, tree, urls):
        raise NotImplementedError("This method should be overriden in subclass")

    @abstractmethod
    def fetch_url(self):
        raise NotImplementedError("This method should be overriden in subclass")