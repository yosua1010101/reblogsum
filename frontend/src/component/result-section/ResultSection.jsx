import React from 'react'

function SummaryBox({blogId, blogTitle, summaryParagraph, timestamp}){
    return (
        <>
        <div className="my-3"> 
            <article id={`summary-${blogId}`}>
                <details>
                    <summary>{`${blogTitle} - ${timestamp}`}</summary>
                    <p>{summaryParagraph}</p>
                </details>
            </article>
        </div>
        <br />
        </>
    )
}



export default function ResultSection({summaryList}){
    var summaryBoxes = []
    summaryList.forEach((element)=>{
        var blogId = element['blog_id']
        var blogTitle = element['title']
        var summaryParagraph = element['summary']
        var timestamp = new Date(...element['timestamp'])
        summaryBoxes.push(<SummaryBox blogId={blogId} blogTitle={blogTitle} summaryParagraph={summaryParagraph} timestamp={timestamp}/>)
    })
    return(
        <>
        <p>Click the arrow to show the summary</p>
        <br />
        {summaryBoxes}
        </>
    )
}