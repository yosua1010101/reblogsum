import React, {useState, useRef} from 'react'
import FormSection from './input-form/FormSection'
import ResultSection from './result-section/ResultSection'

export default function PageContainer({apiUrl}){
    const [section, setSection] = useState("form")
    const data = useRef(null)

    const changePage = (jsonstr)=> {
        let obj = jsonstr
        data.current = obj
        console.log(data.current)
        setSection("result")
    }

    return(
        <>
        <div className="container my-3 bg-warning">
            <h1 className="text-center">ReBlogSum</h1>
            <p className="text-center"> ReBlogSum is a summarization platform to process live blog data </p>
            <br />
            {section === "form" && <FormSection handleSectionChange={(jsonstr)=>changePage(jsonstr)} apiUrl={apiUrl} />}
            {section === "result" && <ResultSection summaryList={data.current['data']} apiUrl={apiUrl}/>}
        </div>
        </>
    )
}