import React from 'react'
import axios from 'axios'
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";

export default function FormSection({handleSectionChange, apiUrl}){
    const [beginDate, setBeginDate] = React.useState(new Date())
    const [endDate, setEndDate] = React.useState(new Date())
    const [dateSent, setDateSent] = React.useState(false)

    const handleSubmit = async(event)=>{
        setDateSent(true)
        event.preventDefault()
        const jsonbuf = await axios(
            {
                method: 'post',
                url: apiUrl + 'upload',
                data: {
                    lowerRange: beginDate.toISOString().split('T')[0],
                    upperRange: endDate.toISOString().split('T')[0]
                }
            }
        )
        handleSectionChange(jsonbuf.data);        
    }

    return(
        <form className="container d-grid gap-2" onSubmit={handleSubmit}>
            <fieldset>
                <legend>Pick a Date Range:</legend>
                <label>Begin Date</label>
                <DatePicker selected={beginDate} onChange={(date) => setBeginDate(date)} />
                <label>End Date</label>
                <DatePicker selected={endDate} onChange={(date) => setEndDate(date)} />
                <div className="d-grid d-flex justify-content-center">
                    <button type="button" id="resetButton" className="btn btn-secondary m-3" disabled={dateSent}
                            onClick={(e)=>{
                                setBeginDate(new Date());
                                setEndDate(new Date());
                                }}>
                        Reset
                    </button>
                    <button id="submitButton" className="btn btn-secondary m-3" type="submit" disabled={dateSent}>
                        {dateSent?"Sending...":"Submit"}
                    </button>
                </div>
            </fieldset>
        </form>
    )

}